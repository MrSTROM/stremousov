﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Свойств_и_методы
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
            bt1.BackColor = Color.White;
            bt1.Parent = this;
            bt1.Location = new Point(100, 30);
            bt1.Text = "Привет";
            bt1.MouseMove += Bt1_MouseMove;
            bt1.MouseLeave += Bt1_MouseLeave;
            bt1.FlatStyle = FlatStyle.Flat;
            bt1.Size = new Size(80, 25);
            bt1.Click += Bt1_Click;
            bt2.Location = new Point(200, 200);
            bt2.Parent = this;
            bt2.Text = ">";
            bt2.FlatStyle = FlatStyle.Flat;
            bt2.MouseLeave += Bt2_MouseLeave;
            bt2.Size = new Size(25,25);
            bt2.MouseMove += Bt2_MouseMove;
            bt2.Click += Bt2_Click;
            bt3.BackColor = Color.White;
            bt3.Parent = this;
            bt3.Size = new Size(25, 25);
            bt3.Location = new Point(100,200);
            bt3.Text = "Х";
            bt3.MouseMove += Bt3_MouseMove;
            bt3.MouseLeave += Bt3_MouseLeave;
            bt3.Click += Bt3_Click;
            bt3.FlatStyle = FlatStyle.Flat;
        }
        Form2 f2 = new Form2();
        private void Bt3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Bt3_MouseLeave(object sender, EventArgs e)
        {
            bt3.BackColor = Color.White;
            bt3.ForeColor = Color.Black;
            bt3.Size = new Size(25, 25);
        }

        private void Bt3_MouseMove(object sender, MouseEventArgs e)
        {
            bt3.BackColor = Color.Bisque;
            bt3.Size = new Size(30, 30);
            bt3.ForeColor = Color.SaddleBrown;
        }

        private void Bt2_Click(object sender, EventArgs e)
        {
            this.Hide();
            
            f2.Show();
        }

        private void Bt2_MouseMove(object sender, MouseEventArgs e)
        {
            bt2.BackColor = Color.Bisque;
            bt2.Size = new Size(30, 30);
            bt2.ForeColor = Color.SaddleBrown;
        }

        private void Bt2_MouseLeave(object sender, EventArgs e)
        {
            bt2.BackColor = Color.White;
            bt2.ForeColor = Color.Black;
            bt2.Size = new Size(25, 25);
        }
        private void Bt1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Здравствуйте " + SystemInformation.UserName);
        }

        private void Bt1_MouseLeave(object sender, EventArgs e)
        {
            bt1.BackColor = Color.White;
            bt1.ForeColor = Color.Black;
            bt1.Size = new Size(80, 25);
        }

        private void Bt1_MouseMove(object sender, MouseEventArgs e)
        {
            bt1.BackColor = Color.Bisque;
            bt1.Size = new Size(85,30);
            bt1.ForeColor = Color.SaddleBrown;
        }

        Button bt2 = new Button();
        Button bt1 = new Button();
        Button bt3 = new Button();
      
    }
}
