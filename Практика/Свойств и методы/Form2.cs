﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Свойств_и_методы
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            bt1.BackColor = Color.White;
            bt1.Parent = this;
            bt1.Location = new Point(100, 50);
            bt1.Text = "Выполнить";
            bt1.MouseMove += Bt1_MouseMove;
            bt1.MouseLeave += Bt1_MouseLeave;
            bt1.FlatStyle = FlatStyle.Flat;
            bt1.Size = new Size(80, 25);
            bt1.Click += Bt1_Click;
            bt2.Location = new Point(300, 200);
            bt2.Parent = this;
            bt2.Text = "<";
            bt2.FlatStyle = FlatStyle.Flat;
            bt2.MouseLeave += Bt2_MouseLeave;
            bt2.Size = new Size(25, 25);
            bt2.MouseMove += Bt2_MouseMove;
            bt2.Click += Bt2_Click;
            list1.BackColor = Color.LightGray;
            list1.Location = new Point(200,50);
            list1.Parent = this;
            list1.AutoSize = true;
            bt3.BackColor = Color.White;
            bt3.Parent = this;
            bt3.Size = new Size(25, 25);
            bt3.Location = new Point(100, 200);
            bt3.Text = "Х";
            bt3.MouseMove += Bt3_MouseMove;
            bt3.MouseLeave += Bt3_MouseLeave;
            bt3.Click += Bt3_Click;
            bt3.FlatStyle = FlatStyle.Flat;

        }

        private void Bt3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Bt3_MouseLeave(object sender, EventArgs e)
        {
            bt3.BackColor = Color.White;
            bt3.ForeColor = Color.Black;
            bt3.Size = new Size(25, 25);
        }

        private void Bt3_MouseMove(object sender, MouseEventArgs e)
        {
            bt3.BackColor = Color.Bisque;
            bt3.Size = new Size(30, 30);
            bt3.ForeColor = Color.SaddleBrown;
        }

        private void Bt2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void Bt2_MouseMove(object sender, MouseEventArgs e)
        {
            bt2.BackColor = Color.Bisque;
            bt2.Size = new Size(30, 30);
            bt2.ForeColor = Color.SaddleBrown;
        }

        private void Bt2_MouseLeave(object sender, EventArgs e)
        {
            bt2.BackColor = Color.White;
            bt2.ForeColor = Color.Black;
            bt2.Size = new Size(25, 25);
        }

        private void Bt1_Click(object sender, EventArgs e)
        {
            int[] mass = new int[10];
            Random rnd = new Random();
            for (int i = 0; i < mass.Length; i++)
            {
                mass[i] = rnd.Next(0, 20);
                list1.Items.Add(Convert.ToString(mass[i]));
            }
        }

        private void Bt1_MouseLeave(object sender, EventArgs e)
        {
            bt1.BackColor = Color.White;
            bt1.ForeColor = Color.Black;
            bt1.Size = new Size(80, 25);
        }

        private void Bt1_MouseMove(object sender, MouseEventArgs e)
        {
            bt1.BackColor = Color.Bisque;
            bt1.Size = new Size(85, 30);
            bt1.ForeColor = Color.SaddleBrown;
        }
        ListBox list1 = new ListBox();
        Button bt1 = new Button();
        Button bt2 = new Button();
        Button bt3 = new Button();
    }
}
